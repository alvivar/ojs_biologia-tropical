{**
 * index.tpl
 *
 * Copyright (c) 2003-2012 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Journal index page.
 *
 * $Id$
 *}
{strip}
{assign var="pageTitleTranslated" value=$siteTitle}
{** gekohack {include file="common/header.tpl"} *}
{include file="gekobt/headerHome.tpl"}
{/strip}

{** gekohack *}
{include file="gekobt/journalHome.tpl"}

<!-- gekohack
{if $issue && $currentJournal->getSetting('publishingMode') != $smarty.const.PUBLISHING_MODE_NONE}
	{* Display the table of contents or cover page of the current issue. *}
	<br />
	<h3>{$issue->getIssueIdentification()|strip_unsafe_html|nl2br}</h3>
	{include file="issue/view.tpl"}
{/if}
-->

{** gekohack {include file="common/footer.tpl"} *}
{include file="gekobt/footer.tpl"}
