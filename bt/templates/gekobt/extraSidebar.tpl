<div id="extraSidebar">

	{if ($currentJournal)}

		<div id="extraLogo">
			<img src="{$baseUrl}/plugins/themes/gekoBT/images/abejorro.png">
		</div>

		<div style="clear:both"></div>

		<div id="extraSocial">

			<div class="socialBlock">
				<a href="http://www.facebook.com/revista.biologia.tropical">
					<img 
						src="{$baseUrl}/plugins/themes/gekoBT/images/fb.png" 
						onmouseover="this.src='{$baseUrl}/plugins/themes/gekoBT/images/fb_a.png'"
						onmouseout="this.src='{$baseUrl}/plugins/themes/gekoBT/images/fb.png'">
				</a>	
			</div>

			<div class="socialBlock">
				<a href="http://www.twitter.com/biologiatropical">
					<img 
						src="{$baseUrl}/plugins/themes/gekoBT/images/tw.png" 
						onmouseover="this.src='{$baseUrl}/plugins/themes/gekoBT/images/tw_a.png'"
						onmouseout="this.src='{$baseUrl}/plugins/themes/gekoBT/images/tw.png'">
				</a>
			</div>

			<div class="socialBlock">
				<a href="{url page="gateway" op="plugin" path="WebFeedGatewayPlugin"|to_array:"rss2"}">
					<img 
						src="{$baseUrl}/plugins/themes/gekoBT/images/rss.png" 
						onmouseover="this.src='{$baseUrl}/plugins/themes/gekoBT/images/rss_a.png'"
						onmouseout="this.src='{$baseUrl}/plugins/themes/gekoBT/images/rss.png'">
				</a>
			</div>

		</div>	

	{/if}

</div>
