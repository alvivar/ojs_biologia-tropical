{**
 * footer.tpl
 *
 * Copyright (c) 2000-2012 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site footer.
 *
 *}
{if $displayCreativeCommons}
{translate key="common.ccLicense"}
{/if}
{if $pageFooter}
<br /><br />

	</div>
	</div>

	{if ($currentJournal)}

		<div id="footer_bt">
			{include file="gekobt/navbarBottom.tpl"}
			<div id="footer_bt_content">
				{$pageFooter}
				{/if}
				{call_hook name="Templates::Common::Footer::PageFooter"}
			</div>
			<div id="footer_bar">
				<img src="{$baseUrl}/plugins/themes/gekoBT/images/footer-bar.png">
			</div>
			<div id="footer_bt_content">
				<p>
				Universidad de Costa Rica - Teléfonos: 2511-5500 - Fax: 2511-5550 - Correo-e: biologia.tropical@ucr.ac.cr
				</p>
			</div>
		</div>

	{/if}
	
	<div>
	<div>

</div><!-- content -->
</div><!-- main -->

</div><!-- body -->

{get_debug_info}
{if $enableDebugStats}{include file=$pqpTemplate}{/if}

</div><!-- container -->
{if !empty($systemNotifications)}
	{translate|assign:"defaultTitleText" key="notification.notification"}
	<script type="text/javascript">
	<!--
	{foreach from=$systemNotifications item=notification}
		{literal}
			$.pnotify({
				pnotify_title: '{/literal}{if $notification->getIsLocalized()}{translate|escape:"js"|default:$defaultTitleText key=$notification->getTitle()}{else}{$notification->getTitle()|escape:"js"|default:$defaultTitleText}{/if}{literal}',
				pnotify_text: '{/literal}{if $notification->getIsLocalized()}{translate|escape:"js" key=$notification->getContents() param=$notification->getParam()}{else}{$notification->getContents()|escape:"js"}{/if}{literal}',
				pnotify_addclass: '{/literal}{$notification->getStyleClass()|escape:"js"}{literal}',
				pnotify_notice_icon: 'notifyIcon {/literal}{$notification->getIconClass()|escape:"js"}{literal}'
			});
		{/literal}
	{/foreach}
	// -->
	</script>
{/if}{* systemNotifications *}
</body>
</html>

