{php} include('config.gk.php'); {/php}

<div id="whyHome">
	<div id="whyPublish">
		<div class="whyTitle">
			<span>
				Why
				<br/>
				<span class="whyBold">publish</span><span class="whyLow">in</span><span class="whyBold">RBT</span>?
			</span>
		</div>
		<div class="whyBlock">
			<ul>
				<li>
					<span>
						Fully <strong>indexed journal</strong>: the Revista is included in Google Scholar, Current Contents, Science Citation Index, Biological Abstracts, etc.
					</span>
				</li>
				<li>
					<span>
						The publication <strong>does not require a mandatory payment</strong> by the authors.
					</span>
				</li>
				<li>
					<span>
						Articles are freely available online <strong>(Open Access)</strong> so they are more likely to be cited than articles from pay-only journals.
					</span>
				</li>
			</ul>
		</div>
		<div class="whyBlock">
			<ul>
				<li>
					<span>
						Rapid article processing and publication (<strong>4-8 months</strong> from submission to publication).
					</span>
				</li>
				<li>
					<span>
						<strong>Widely read</strong> in countries with the highest biodiversity, where your article can have the greatest impact.
					</span>
				</li>
				<li>
					<span>
						A dedicated staff gives you a <strong>personalized treatment</strong>.
					</span>
				</li>
			</ul>
		</div>
	</div>
	<div style="clear:both"></div>
	<div id="whySecond">
		<div id="whySubmit">
			<div class="whyTitle">
				<span class="whyBold">				
					Submit
				</span>
			</div>
			<div class="whyBlock">
				<p>
					Before sending a manuscript to our journal, we recommend you to carefully review that details of format and style are according to our requirements. Once you have taken this into account please send your work to the e-mail address 
					<a href="mailto:biologia.tropical@ucr.ac.cr">biologia.tropical@ucr.ac.cr</a>.
				</p>
			</div>
			<div class="whyLinksList">
				<ul>
					<li>
						<span>
							<a href="{$baseUrl}/files/docs/submit/Authors_guide.pdf">Author's guide</a>
						</span>
					</li>
					<li>
						<span>
							<a href="{$baseUrl}/files/docs/submit/EnglishFormatExample.pdf">Sample article</a>
						</span>
					</li>
					<li>
						<span>
							<a href="{$baseUrl}/files/docs/submit/guia_figuras.pdf">Figures guide</a>
						</span>
					</li>
					<li>
						<span>
							<a href="{url page="pages" op="view"}/submit-content">More details about the publication process</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="whyScope">
			<div class="whyTitle">
				<span>
					Our <span class="whyBold">scope</span>
				</span>
			</div>
			<div class="whyBlock">
				<p>
					Our journal publishes articles in all fields of <strong>tropical biology</strong> and <strong>conservation</strong>.
				</p>
				<p>
					Selection criteria are the amount of <strong>new information, quality</strong> and <strong>interest to a general readership</strong>. Studies with a strong experimental design, prolongued field work, and taxonomic-systematics studies of whole groups are considered.
				</p>
				<p>
					Notes, short communications, range extensions and similar short studies <strong>are no longer accepted</strong>. 
				</p>
			</div>
		</div>
		<div id="whyPapers">
			<div class="whyTitle">
				<span>
					Read <span class="whyBold">papers</span>
				</span>
			</div>
			<div class="whyBlock">
				<p>
					Our journal agrees with the norms of the <strong>Open Access Initiative</strong>, every published paper can be read or download from this website.
				</p>
				<p>
				 	Puedes leer nuestra &uacute;ltima publicaci&oacute;n en este <a href="{url page="issue" op="current" path=""}">enlace</a>.
				</p>	
			</div>
			<div class="whyLinksList">
				<ul>

				{php}

					$mysqli = new mysqli($server, $dbuser, $dbpass, $dbbase);

					if ($mysqli->connect_errno) {
		    			exit();
					}

					if ($result = $mysqli->query(
						"SELECT * FROM `article_settings`
						WHERE `setting_name` = 'cleanTitle'
						GROUP BY `article_id`
						ORDER BY `article_id`
						DESC LIMIT 3"))
					{

						$url = '{url page="pages" op="view"}';

						while($row = mysqli_fetch_assoc($result)) {
							echo "<li><span>";
							{/php}
							<a href="{url page="article" op="view"}/{php} echo $row['article_id']; {/php}">
								{php} echo substr( $row['setting_value'], 0, 70 ), ".."; {/php}
							</a>
							{php}
		            		echo "</span></li>";
		        		}
					}

					$result->close();

				{/php}

				</ul>
			</div>
		</div>
	</div>
</div>
<div style="clear:both;"> </div>
<div id="whyRecent">
	<div class="whyTitle">
		<span>
			Recent
			<span class="whyBold">Publications</span>
		</span>
	</div>

	{php}

		$mysqli = new mysqli($server, $dbuser, $dbpass, $dbbase);

		if ($mysqli->connect_errno) {
			exit();
		}

		if ($result = $mysqli->query(
			"SELECT * FROM `article_settings`
			WHERE `setting_name` = 'cleanTitle'
			GROUP BY `article_id`
			ORDER BY `article_id`
			DESC LIMIT 4"))
		{

			$url = '{url page="pages" op="view"}';

			$odd = false;

			while($row = mysqli_fetch_assoc($result)) {

				{/php}

					<div class="whyRecentBlock {php} if ($odd == true) { echo "backgroundWhite"; } {/php} ">
						<div class="whyRecentContent">
							<a href="{url page="article" op="view"}/{php} echo $row['article_id']; {/php}">
								{php}

									echo $row['setting_value'];

								{/php}
							</a>
							<div class="whyRecentAuthor">
								{php}

								if ($result_author = $mysqli->query(
									"SELECT * FROM `authors` WHERE `submission_id` = {$row['article_id']}"))
								{
									while($row_author = mysqli_fetch_assoc($result_author)) {
										echo $row_author['first_name'], " ";
										echo $row_author['middle_name'], " ";
										echo $row_author['last_name'];
										if ($result_author) 
										{
											echo ', ';
										}
									}
								}

								{/php}
							</div>
						</div>
						<div class="whyRecentPDF">							
							<a href="{url page="article" op="view"}/{php} echo $row['article_id'], '/', $row['article_id']; {/php}">
								PDF
							</a>
						</div>
						 <div style="clear:both"></div>
					</div>

				{php}

				if ($odd == false) 
				{
					$odd = true;
				}
				else 
				{
					$odd = false;
				}

    		}
		}

		$result->close();

	{/php}

</div>
