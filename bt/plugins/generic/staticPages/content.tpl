{**
 * plugins/generic/staticPages/content.tpl
 *
 * Copyright (c) 2003-2012 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Display Static Page content
 *
 * $Id$
 *}
{assign var="pageTitleTranslated" value=$title}
{include file="gekobt/headerHome.tpl"}

{$content}

{include file="common/footer.tpl"}
