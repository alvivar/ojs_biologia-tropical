<?php

import('classes.plugins.ThemePlugin');

class GekoBTThemePlugin extends ThemePlugin {
	/**
	 * Get the name of this plugin. The name must be unique within
	 * its category.
	 * @return String name of plugin
	 */
	function getName() {
		return 'GekoBTThemePlugin';
	}

	function getDisplayName() {
		return 'GekoBT Theme';
	}

	function getDescription() {
		return 'GekoBT layout';
	}

	function getStylesheetFilename() {
		return 'gekoBT.css';
	}

	function getLocaleFilename($locale) {
		return null; // No locale data
	}
}

?>
